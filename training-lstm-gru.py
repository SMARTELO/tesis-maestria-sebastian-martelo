import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import warnings
warnings.filterwarnings("ignore")
import logging
logging.disable(logging.CRITICAL)
from keras.models import Sequential
from keras.layers import Dense, LSTM, GRU, Dropout, BatchNormalization
from keras.callbacks import ModelCheckpoint
import pickle
import os
import gc
from keras import backend as K 


#colData = 'VRSN.OQ'
#setData = 'set1'
#modelToTrain = 'LSTM'

epochs = 200
loopBack = 60

inFolder = 'ForDownload'
outFolder = 'outFolder'

setList = ['set'+str(i) for i in range(1,6)]
#setList.reverse()
tickers = ['VRSN.OQ', 'MSFT.OQ', 'AAPL.OQ', 'PCAR.OQ', 'MXIM.OQ','PEP.OQ', 'INTC.OQ', 'EBAY.OQ', 'ANSS.OQ', 'FOX.OQ']
tickers.reverse()

als = ['LSTM', 'GRU']
for setData in setList:
    for colData in tickers:
        for modelToTrain in als:
            print(f"*** {setData} {colData} {modelToTrain} ***")

            #Crea carpeta
            try:
                os.mkdir(outFolder)
            except FileExistsError:
                pass
            filesPrefix = setData+'_'+colData+'_'+modelToTrain

            #Lee archivos
            dfTrain = pd.read_csv(inFolder+'/'+setData+'_train_selected.csv')
            dfTest = pd.read_csv(inFolder+'/'+setData+'_test_selected.csv')

            #Filter data
            dfTrain['Date_Time2'] = pd.to_datetime(dfTrain['Date_Time'])
            dfTrain = dfTrain[(dfTrain['Date_Time2'].dt.hour<=19) & (dfTrain['Date_Time2'].dt.hour>=4)]

            dfTest['Date_Time2'] = pd.to_datetime(dfTest['Date_Time'])
            dfTest = dfTest[(dfTest['Date_Time2'].dt.hour<=19) & (dfTest['Date_Time2'].dt.hour>=4)]

            dfTrain = dfTrain[['Date_Time','Date_Time2',colData]].rename(columns={colData: 'value'})
            dfTest = dfTest[['Date_Time','Date_Time2',colData]].rename(columns={colData: 'value'})

            mask = (dfTrain['value']-dfTrain['value'].shift(1))!=0
            dfTrain = dfTrain[mask]
            mask = (dfTest['value']-dfTest['value'].shift(1))!=0
            dfTest = dfTest[mask]

            #Calcula retorno locaritmico
            dfTrain['log_diff'] = np.log(dfTrain['value']/dfTrain['value'].shift(1)).bfill()
            dfTest['log_diff'] = np.log(dfTest['value']/dfTest['value'].shift(1)).bfill()

            #Calcula parametros de escalado y normalizacion
            cols = ['log_diff']
            colsOut = ['log_diff_scaled']

            muScale = dfTrain[cols].mean(axis=0)
            stdScale = dfTrain[cols].std(axis=0)

            dfTrain[colsOut] = (dfTrain[cols]-muScale[cols])/stdScale[cols]
            dfTest[colsOut] = (dfTest[cols]-muScale[cols])/stdScale[cols]

            #Ventana temporal
            loopBackCols = '_lb_'
            colName = 'log_diff_scaled'
            features = [colName]
            for i in range(1,loopBack+1):
                dfTrain[colName+loopBackCols+str(i)] = dfTrain[colName].shift(i).fillna(0)
                dfTest[colName+loopBackCols+str(i)] = dfTest[colName].shift(i).fillna(0)
                features.append(colName+loopBackCols+str(i))

            #Asigna target
            dfTrain['taget'] = dfTrain['log_diff_scaled'].shift(-1).ffill()
            dfTest['taget'] = dfTest['log_diff_scaled'].shift(-1).ffill()
            target = ['taget']    

            #Separa training y testing
            X_train, X_test = dfTrain[features].values, dfTest[features].values
            Y_train, Y_test = dfTrain[target], dfTest[target]

            X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))
            Y_train = np.reshape(Y_train, (Y_train.shape[0], 1))

            X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
            Y_test = np.reshape(Y_test, (Y_test.shape[0], 1))
            
            #print(X_train)
            #print(Y_train)

            #Modelos Y entrenamiento
            if modelToTrain == 'LSTM':
                dim_entrada = (X_train.shape[1], 1)
                dim_salida = 1

                modelo = Sequential()
                modelo.add(LSTM(50, input_shape=dim_entrada, return_sequences=True))
                modelo.add(BatchNormalization())
                modelo.add(Dropout(0.1))
                
                modelo.add(LSTM(50, input_shape=(50,1)))
                modelo.add(BatchNormalization())
                modelo.add(Dropout(0.1))
                
                modelo.add(Dense(dim_salida, activation='tanh'))
                modelo.compile(loss='mse', optimizer='adam', metrics=['accuracy'])

            elif(modelToTrain == 'GRU'):
                dim_entrada = (X_train.shape[1], 1)
                dim_salida = 1

                modelo = Sequential()
                modelo.add(GRU(50, input_shape=dim_entrada, return_sequences=True))
                modelo.add(BatchNormalization())
                modelo.add(Dropout(0.1))
                
                modelo.add(GRU(50, input_shape=(50,1)))
                modelo.add(BatchNormalization())
                modelo.add(Dropout(0.1))
                
                modelo.add(Dense(dim_salida, activation='tanh'))
                modelo.compile(loss='mse', optimizer='adam', metrics=['accuracy'])

            filepath = outFolder+'/'+filesPrefix+'_model.h5'
            checkpoint = ModelCheckpoint(filepath, monitor='loss', verbose=1, save_best_only=True, mode='min')
            callbacks_list = [checkpoint]
            
            
            history = modelo.fit(X_train, Y_train, 
                                 epochs=epochs, 
                                 batch_size=2048, 
                                 validation_data=(X_test, Y_test),
                                 callbacks=callbacks_list)
            
            with open(outFolder+'/'+filesPrefix+'_history.pk', 'wb') as file_pi:
                pickle.dump(history.history, file_pi)


            #Inferencia de fatos
            Y_train_predict = modelo.predict(X_train)
            Y_test_predict = modelo.predict(X_test)
            
            #print(Y_train_predict)
            #print(sum(Y_train_predict))
            #print("----*****")
            #print(Y_test_predict)
            #print(sum(Y_test_predict))
            #raise ValueError("Date provided can't be in the past")

            Y_train_predict = Y_train_predict * stdScale['log_diff'] + muScale['log_diff']
            Y_test_predict = Y_test_predict * stdScale['log_diff'] + muScale['log_diff']

            #Post Procesamiento
            dfTrainOut = dfTrain[['Date_Time','Date_Time2', 'value','log_diff']].copy()
            dfTrainOut['log_diff_predict'] = Y_train_predict
            dfTrainOut['value_predict'] = np.exp(dfTrainOut['log_diff_predict']) * dfTrainOut['value'].shift(1).bfill().ffill()

            dfTestOut = dfTest[['Date_Time','Date_Time2', 'value','log_diff']].copy()
            dfTestOut['log_diff_predict'] = Y_test_predict
            dfTestOut['value_predict'] = (np.exp(dfTestOut['log_diff_predict']) * dfTestOut['value'].shift(1)).bfill().ffill()

            dfTrainOut['train'] = True
            dfTestOut['train'] = False
            cols = ['Date_Time2','value', 'value_predict','log_diff', 'log_diff_predict', 'train']
            dfOut = pd.concat([dfTrainOut[cols], dfTestOut[cols]])

            dfOut.to_csv(outFolder+'/'+filesPrefix+'_data_out.csv')

            #Calculo de errores
            class ErrorMetrics():
                @staticmethod
                def __calculateMape(y_true, y_pred):
                    """ Calculate mean absolute percentage error (MAPE)"""
                    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

                @staticmethod
                def __calculateMpe(y_true, y_pred):
                    """ Calculate mean percentage error (MPE)"""
                    return np.mean((y_true - y_pred) / y_true) * 100

                @staticmethod
                def __calculateMae(y_true, y_pred):
                    """ Calculate mean absolute error (MAE)"""
                    return np.mean(np.abs(y_true - y_pred)) * 100

                @staticmethod
                def __calculateRmse(y_true, y_pred):
                    """ Calculate root mean square error (RMSE)"""
                    return np.sqrt(np.mean((y_true - y_pred)**2))

                @staticmethod
                def __calculateR2(y_true, y_pred):
                    """ Calculate R2 (R2)"""
                    y_mu = np.mean(y_true)
                    return 1-(np.sum((y_true - y_pred)**2))/(np.sum((y_true - y_mu)**2))


                @staticmethod
                def generateMetrics(y_true, y_pred):
                    return {
                        'MAPE': ErrorMetrics.__calculateMape(y_true, y_pred),
                        'MPE': ErrorMetrics.__calculateMpe(y_true, y_pred),
                        'MAE': ErrorMetrics.__calculateMae(y_true, y_pred),
                        'RMSE': ErrorMetrics.__calculateRmse(y_true, y_pred),
                        'R2': ErrorMetrics.__calculateR2(y_true, y_pred),
                    }

                @staticmethod
                def printErrorMetrics(d):
                    for k in d.keys():
                        print(f'{k}: {d[k]}')

            errors = ErrorMetrics.generateMetrics(dfTestOut['value_predict'], dfTestOut['value'])
            with open(outFolder+'/'+filesPrefix+'_errors.pk', 'wb') as file_pi:
                pickle.dump(errors, file_pi)
                
            K.clear_session()
            gc.collect()
            del modelo
            